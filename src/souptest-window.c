/* souptest-window.c
 *
 * Copyright 2019 Daniel García Moreno
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "souptest-config.h"
#include "souptest-window.h"
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>

struct _SouptestWindow
{
  GtkApplicationWindow  parent_instance;

  /* Template widgets */
  GtkHeaderBar        *header_bar;
  GtkLabel            *label;
};

G_DEFINE_TYPE (SouptestWindow, souptest_window, GTK_TYPE_APPLICATION_WINDOW)

static void
souptest_window_class_init (SouptestWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Souptest/souptest-window.ui");
  gtk_widget_class_bind_template_child (widget_class, SouptestWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, SouptestWindow, label);
}

static void
element_cb (JsonArray *array,
            guint      index,
            JsonNode  *element,
            gpointer   data)
{
  JsonObject *object = json_node_get_object  (element);
  printf ("%s: %s\n",
          json_object_get_string_member (object, "id"),
          json_object_get_string_member (object, "description")
          );
}

static void
request_callback (GObject *object, GAsyncResult *result, gpointer user_data)
{
  g_autoptr(JsonParser) parser = json_parser_new ();
  g_autoptr(GInputStream) stream;
  JsonNode *node = NULL;
  JsonArray *array = NULL;
  
  stream = soup_session_send_finish (SOUP_SESSION (object), result, NULL);
  
  json_parser_load_from_stream (parser, stream, NULL, NULL);
  
  node = json_parser_get_root (parser); 
  array = json_node_get_array (node); 
  
  json_array_foreach_element (array, element_cb, NULL);

  /* gssize size = 0; */
  /* gchar buffer[1024]; */
  
  /* printf ("buffer: \n"); */
  /* size = g_input_stream_read (stream, buffer, 1024, NULL, NULL); */
  /* while (size) */
  /*   { */
  /*     printf ("%s", buffer); */
  /*     size = g_input_stream_read (stream, buffer, 1024, NULL, NULL); */
  /*     buffer[size] = 0; */
  /*   } */
  /*   printf ("\n"); */
  
}

static GFile *
get_url (const char *url, GError **error)
{
  g_autoptr (SoupMessage) msg;
  GFile *output_file = NULL;
  GFileIOStream *iostream = NULL;
  g_autoptr(SoupSession) session = soup_session_new ();
  GOutputStream *output = NULL;
  gsize bytes = 0;

  msg = soup_message_new ("GET", url);
  soup_session_send_message (session, msg);

	if (SOUP_STATUS_IS_SUCCESSFUL (msg->status_code))
    {
      output_file = g_file_new_tmp (NULL, &iostream, error);
      if (*error != NULL)
        {
          g_object_unref (output_file);
          return NULL;
        }

      output = g_io_stream_get_output_stream (G_IO_STREAM (iostream));
      g_output_stream_write_all (output,
                           msg->response_body->data,
                           msg->response_body->length,
                           &bytes,
                           NULL,
                           error);

      if (*error != NULL)
        {
          g_object_unref (output_file);
          return NULL;
        }
    }
  return output_file;
}

static void
souptest_window_init (SouptestWindow *self)
{
  g_autoptr(SoupSession) session = soup_session_new ();
  g_autoptr(SoupMessage) message = soup_message_new ("GET", "https://l10n.gnome.org/teams/json");
  GError *error = NULL;
  
  g_autoptr(GFile) file = get_url ("https://l10n.gnome.org/teams/json", &error);
  if (error != NULL)
    {
      fprintf (stderr, "Unable to GET the file: %s\n", error->message);
      g_error_free (error);
    }
  g_autoptr(GFile) dest = g_file_new_for_uri ("file:///tmp/test.json");
  g_file_copy (file, dest, G_FILE_COPY_OVERWRITE, NULL, NULL, NULL, &error);
  if (error != NULL)
    {
      fprintf (stderr, "Unable to copy file: %s\n", error->message);
      g_error_free (error);
    }

  //soup_session_send_async (session, message, NULL, request_callback, NULL);
  
  gtk_widget_init_template (GTK_WIDGET (self));
}
